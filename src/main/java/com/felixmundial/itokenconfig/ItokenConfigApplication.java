package com.felixmundial.itokenconfig;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ItokenConfigApplication {

    public static void main(String[] args) {
        SpringApplication.run(ItokenConfigApplication.class, args);
    }

}
